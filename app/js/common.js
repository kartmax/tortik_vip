// Custom JS
// ==================================================

// preloader
window.onload = function () {
    document.body.classList.add('loaded_hiding');
    window.setTimeout(function () {
        document.body.classList.add('loaded');
        document.body.classList.remove('loaded_hiding');
    }, 500);
}

// ==================================================
// End Custom JS



// Custom jQ
// ==================================================

$(function() {
    //menu
    // $('#block-menu').append('<div class="mob-btn cbutton cbutton--effect-nikola"><svg class="icon btn-menu"><use xlink:href="img/svg/symbols.svg#btn-menu"></use></svg></div>');
    $('.mob-btn').click(function () {
        $(this).toggleClass('active-mob-btn');
        $('.menu').slideToggle(200);
    });

    $('#block-menu ul').parent('li').addClass('has-submenu');
    $('#block-menu li ul').addClass('submenu');

    $('#block-menu .has-submenu').hover(function () {
       $(this).children('.submenu').slideToggle(250);
    });

    //tabs
    function initTabs() {
        const $ul_tabs_caption = $('ul.tabs__caption')

        // первый элемент при открытии всегда должен быть активным
        $ul_tabs_caption.find('li').removeClass('active')
        var $first_tab = $ul_tabs_caption.find('li:first')
        $first_tab.addClass('active')
        $first_tab.closest('div.tabs').find('div.tabs__content').removeClass('active').eq( $first_tab.index()).addClass('active');

        $ul_tabs_caption.on('click', 'li:not(.active)', function() {
            $(this)
                .addClass('active').siblings().removeClass('active')
                .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
        });
    }

    // fullPage
    // $('#fullpage').fullpage();

    // wow
    new WOW().init();

    // for developing
    // const DOMAIN_PORT = 'http://127.0.0.1:8000';

    // for relise
    const DOMAIN_PORT = 'https://api.tortik.vip'

    // ПОЛУЧЕНИЕ КОЛИЧЕСТВА ОТЗЫВОВ ПО id товара
    $count_reviews = $('.count-reviews')
    function get_count_reviews(id_product) {
        $count_reviews.html('')
        $.ajax({
            type: "GET",
            url: DOMAIN_PORT + '/api/v1/reviews/counter/product_' + id_product,
            cache: false,
            dataType: 'json',

            complete: function(data) {
                const json = data.responseText
                const obj = $.parseJSON(json)

                const count = obj.count
                if(count > 0) {
                    $count_reviews.removeClass('hidden')
                    $count_reviews.html(count)
                } else {
                    $count_reviews.addClass('hidden')
                }
            }
        })
    }

    // ПОЛУЧЕНИЕ ОТЗЫВОВ ПО id товара
    const $box_reviews = $('.box-reviews')
    function get_reviews_product(id_product) {
        $.ajax({
            type: "GET",
            url: DOMAIN_PORT + '/api/v1/reviews/review/product_' + id_product,
            cache: false,
            dataType: 'json',

            complete: function(data) {
                const json = data.responseText
                const obj = $.parseJSON(json)
                const status = obj.status
                
                var reviews_list = document.createElement('div')
                $(reviews_list).addClass('reviews-list')

                if (status == 200) {
                    const length_obj = obj.result_list.length
                    for (var i = 0; i < length_obj; i++) {
                        var review = document.createElement('div')
                        $(review).addClass('review')
                        $(review).html('<p class="review_author">' + obj.result_list[i].name_author + '<span class="review_date"> - ' + obj.result_list[i].date_create + '</span></p><p class="review_text">' + obj.result_list[i].text + '</p>')
                        $(reviews_list).append($(review))
                    }
                } else { // status 400 - нету отзывов
                    var review = document.createElement('div')
                    $(review).addClass('review')
                    $(review).html('<p class="review_text">Еще никто не написал отзыв. Вы можете стать первым.</p>')
                    $(reviews_list).append($(review))
                }
                $box_reviews.html(reviews_list)
                if(localStorage.name_author) {
                    $box_reviews.append('<form class="review_form"><input type="text" class="review_firstname" name="name_author" placeholder="Ваше имя" value="' + localStorage.name_author + '"><div class="review_message-box"><input type="text" class="review_message" name="text_review" placeholder="Напишите отзыв"><input type="submit" class="btn-send"></div></form>')
                } else {
                    $box_reviews.append('<form class="review_form"><input type="text" class="review_firstname" name="name_author" placeholder="Ваше имя" value=""><div class="review_message-box"><input type="text" class="review_message" name="text_review" placeholder="Напишите отзыв"><input type="submit" class="btn-send"></div></form>')
                }
            }
        })
        return false
    }

    const $tab_get_review = $('.get-reviews')
    $tab_get_review.on('click', function() {
        get_reviews_product(localStorage.id_product)
    })

    // СОЗДАНИЕ ОТЗЫВА
    function create_reviews_product(id, name, text) {
        var formData = new FormData()
        formData.set('product', id)
        formData.set('name_author', name)
        formData.set('text', text)

        $.ajax({
            type: "POST",
            url: DOMAIN_PORT + '/api/v1/reviews/review/create',
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false,
            data: formData,

            complete: function(data) {
                get_reviews_product(id)
                get_count_reviews(id)
            }
        })
        return false
    }

    $(document).on('submit', '.review_form', function(e) {
        e.preventDefault()
        $review_form = $('.review_form')

        if(!$('.review_firstname').val()) {
            $review_form.addClass('validate-name')
            setTimeout(function() {
                $review_form.removeClass('validate-name')
            }, 200)
            return
        }
        if(!$('.review_message').val()) {
            $review_form.addClass('validate-name')
            setTimeout(function() {
                $review_form.removeClass('validate-name')
            }, 200)
            return
        }

        const id_product = localStorage.id_product
        const name_author = $('.review_firstname').val()
        localStorage.name_author = name_author
        const text_comment = $('.review_message').val()

        var top = $('.product-card').scrollTop()
        create_reviews_product(id_product, name_author, text_comment)
        $('.product-card').scrollTop(top)
    })
    

    // SEARCH-функция поиска товара с поля поиска
    // - получение списка товаров через поиск по слову
    const $search_list = $('.popup-search__list')
    const $search_count = $('.popup-search__count').find('span')
    function get_search_product_list(search_world) {
        $.ajax({
            type: "GET",
            url: DOMAIN_PORT + "/api/v1/catalog/product/list?search=" + search_world,
            cache: false,
            dataType: "json",

            complete: function(data) {
                status = data.status
                if (status == 200) {
                    const json = data.responseText
                    const obj = $.parseJSON(json)
                    const length_obj = obj.length

                    $search_list.html('')
                    $search_count.html(length_obj)

                    if (length_obj > 0) {
                        for (var i = 0; i < length_obj; i++) {
                            var item_img = obj[i].photo
                            var item_full_title = obj[i].full_title_ru
                            var item_subgroup = obj[i].subgroup
                            var item_id = obj[i].id

                            var item = document.createElement('li')
                            $(item).addClass('search-list__item').attr('data-product-id', item_id)
                            $(item).append('<div class="search-list__item_bg"><img src="' + item_img + '" alt=""></div>')
                            $(item).append('<div class="search-list__item_content"><p class="search-list__item_title">' + item_full_title + '</p>')
                            // $(item).append('<div class="search-list__item_content"><p class="search-list__item_title">' + item_full_title + '</p><p class="search-list__item_desctiption">' + item_subgroup + '</p></div>')
                            $(item).append('<div class="btn-enter btn-func"><svg class="icon btn-to"><use xlink:href="img/svg/symbols.svg#btn-to"></use></svg></div>')
                       
                            $search_list.append(item)
                        }
                    }
                     else {
                        $search_list.html('<p>Не удалось найти "' + search_world + '"</p>')
                    }
                    $search_form.addClass('active')
                    $overlay.addClass('active')
                    $search_popup.css({'zIndex' : 1000, 'opacity' : 100}).addClass('active')
                }
            }
        })
        return false
    }

    function close_search_popup() {
        $search_popup.css({'opacity' : 0}).removeClass('active')
        $search_form.css({'zIndex' : 1}).removeClass('active')
        $overlay.removeClass('active')
        setTimeout(function(){
            $search_popup.css({'zIndex' : -1})
        }, 310)
    }

    const $search_form = $('.search-form')
    const $search_input = $('.search_input')
    const $search_popup = $('.popup-search')
    const $search_close = $('.popup-search__close')
    $search_form.on('submit', function(event) {
        event.preventDefault()
        const val_search = $search_input.val()

        if (val_search != '') {
            get_search_product_list(val_search)
        }
    })
    $search_close.on('click', function() {
        close_search_popup()
    })
    $('.overlay').on('click', function() {
        close_search_popup()
    })

    // ПОЛУЧЕНИЕ СПИСКА ГРУПП
    // - вывод в разметку
    // - инициализация слайдера
    function get_group_list() {
        var formData = new FormData() // обнуляю формдату
        $.ajax({
            type: "GET",
            url: DOMAIN_PORT + "/api/v1/catalog/group/list",
            cache: false,
            dataType: "json",
            contentType: false,
            processData: false,
            data: formData,
            
            complete: function (data) {
                status = data.status;
                if (status == 200) {
                    const json = data.responseText;
                    const obj = $.parseJSON(json);

                    var empty_elem = '<div class="main-group-products_item main-group-products_item__empty"><div class="main-group-products_item__bg"><img src="img/prevue-group-1.jpg" alt=""></div><p>Empty</p></div>'
                    slickSliderGroup.html(empty_elem)
                    
                    for (var i = 0; i < obj.length; i++) {
                        var id = obj[i].id
                        var alias = obj[i].alias
                        var title_ru = obj[i].title_ru
                        var photo = obj[i].photo
                        var product_elem = document.createElement('div')
                        $(product_elem).addClass('main-group-products_item')
                        if (alias == 'cakes') {
                            $(product_elem).addClass('main-group-products_item__active-desc')
                            localStorage.setItem('id_group', id)
                        }
                        $(product_elem).attr('data-id_group', id)
                        $(product_elem).html('<div class="main-group-products_item__bg"><img src="' + photo + '" alt=""></div><p>' + title_ru + '</p>')
                        slickSliderGroup.append(product_elem)
                    }
                    slickSliderGroup.append(empty_elem)
                    slickSliderGroup.append(empty_elem)
                    if (window.matchMedia("(max-width: 767px)").matches) {
                        initslickSliderGroup()
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
                editTextMessage('Произошла ошибка', 'An error has occurred')
            }
        })
        return false
    }
    get_group_list()


    // Slick-Sliders
    // slider group
    const slickSliderGroup = $('.js-slick-main-group-products')
    const listSubgroupProducts = $('.list-subgroup-products')
    const containerMainGroupProducts = $('.container-main-group-products')
    const containerSubgroupProducts = $('.container-subgroup-products')
    const sidebarProducts = $('.sidebar-products')
    const btnCloseSidebar = $('.btn-close-sidebar')
    const titleGroup = $('.title-group')
    const $productCard = $('.product-card')
    const $galleryProduct = $('.gallery-product')
    const $productCardTabsContent = $('.product-card-tabs').find('.box-tabs-content')
    const $overlay = $('.overlay')
    const $get_reviews = $('.get-reviews')

    function initslickSliderGroup() {
        slickSliderGroup.slick({
            vertical: true,
            verticalSwiping: true,
            slidesToShow: 4,
            infinite: false,
            appendArrows: $('.slick-arrow-container')
        });

        $(slickSliderGroup).slick('slickGoTo', 1) // старт с 1-го слайда
    }

    // при клике на слайде групп
    $(document).on('click', '.slick-slide', function() {
        if ($(this).parents().hasClass('js-slick-main-group-products')) {
            var index_click_slide = $(this).attr('data-slick-index')
            $(slickSliderGroup).slick('slickGoTo', index_click_slide-1)
        }
    })
    // при свайпе слайда групп
    $(slickSliderGroup).on('afterChange', function(event, slick, currentSlide, nextSlide) {
        var customActiveSlide = $('.slick-slide')[currentSlide + 1]
        var id_group_product_active = $(customActiveSlide).find('.main-group-products_item').attr('data-id_group')
        var title_group = $(customActiveSlide).find('.main-group-products_item').find('p').html()
        localStorage.setItem('id_group', id_group_product_active)
        localStorage.setItem('title_group', title_group)
        
        // формирование списка подгрупп товаров
        show_list_subgroup()
    })

    // функция для ФОРМИРОВАНИЯ СПИСКА ПОДГРУПП ТОВАРОВ
    function show_list_subgroup() {
        var formData = new FormData() // обнуляю формдату
        $.ajax({
            type: "GET",
            url: DOMAIN_PORT + "/api/v1/catalog/subgroup/group_" + localStorage.id_group,
            cache: false,
            dataType: "json",
            contentType: false,
            processData: false,
            data: formData,

            complete: function (data) {
                status = data.status;

                if (status == 200) {
                    const json = data.responseText;
                    const obj = $.parseJSON(json);

                    listSubgroupProducts.fadeOut(0)
                    var ul = document.createElement('ul')
                    for(i = 0; i < obj.length; i++) {
                        var id = obj[i].id
                        var alias = obj[i].alias
                        var title_ru = obj[i].title_ru
                        var group = obj[i].group

                        var li = document.createElement('li')
                        $(li).attr('data-id_subgroup', id)
                        $(li).addClass('li-subgroups')
                        $(li).html(title_ru)
                        $(ul).append($(li))
                    }
                    $(listSubgroupProducts).fadeIn(200)
                    $(listSubgroupProducts).html($(ul).html())
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
                editTextMessage('Произошла ошибка', 'An error has occurred')
            }
        })
        return false
    }


    // SLIDER PRODUCTS
    const sliderProducts = $('.slider-products')
    function initSliderProducts (sliderProducts) {
        sliderProducts.slick({
            rows: 4,
            arrows: false,
            dots: true,
            fade: true
        })
    }

    // OWL-CAROUSER slider-product
    function initGaleryProduct() {
        $galleryProduct.owlCarousel({
            // items: 1,
            dots: true,

            responsive:{
                0:{
                    items:1
                },
                640:{
                  items:2
                },
                992:{
                    items:3,
                    nav: true
                }
            }
        })
    }


    // ПРИ КЛИКЕ НА ПОДГРУППЕ
    var countClickSubgroup = 0
    $(document).on('click', '.li-subgroups', function () {

        // то что нужно только на мобильных
        if (window.matchMedia("(max-width: 767px)").matches) {
            containerSubgroupProducts.addClass('active')
            containerMainGroupProducts.addClass('opacity')
            sidebarProducts.addClass('active')
            btnCloseSidebar.addClass('active')
        }

        $('.li-subgroups').removeClass('active')
        $(this).addClass('active')
        titleGroup.html(localStorage.getItem('title_group'))
        var id_subgroup = $(this).attr('data-id_subgroup')
        localStorage.setItem('id_subgroup', id_subgroup)
        localStorage.setItem('title_subgroup', $(this).html())
        if (countClickSubgroup == 0) { // если сайдебар еще не активен
            sliderProducts.fadeOut(0)
            parsProductS()
            sliderProducts.fadeIn(300)
            countClickSubgroup++
            // if (window.matchMedia("(max-width: 767px)").matches) {
            //     initSliderProducts(sliderProducts)
            // }
            
        } else { // если сайдебар уже активен (переходы между подгруппами)
            if (window.matchMedia("(max-width: 767px)").matches) {
                if(sliderProducts.hasClass('slick-initialized')) {
                    sliderProducts.slick('unslick') // деактивирую слайдер чтобы активировать его с новым набором данных
                }
                sliderProducts.fadeOut(0)
                parsProductS()
                sliderProducts.fadeIn(300)
                // initSliderProducts(sliderProducts)
            }
            if (window.matchMedia("(min-width: 768px)").matches) {
                sliderProducts.fadeOut(0)
                parsProductS()
                sliderProducts.fadeIn(300)
            }
        }
    })
    btnCloseSidebar.click(function() {
        sidebarProducts.removeClass('active')
        $(this).removeClass('active')
        containerSubgroupProducts.removeClass('active')
        containerMainGroupProducts.removeClass('opacity')
        if(sliderProducts.hasClass('slick-initialized')) {
            sliderProducts.slick('unslick')

        }
        $('.li-subgroups').removeClass('active')
    })


    // ПРИ КЛИКЕ НА ТОВАРЕ (в каталоге или в списке поиска)
    $(document).on('click', '.slider-products-item, .search-list__item', function() {
        localStorage.setItem('id_product', $(this).attr('data-product-id'))
        parsProduct()
        get_count_reviews(localStorage.id_product)

        $productCard.addClass('active')
        $search_form.removeClass('active')
        if (window.matchMedia("(max-width: 767px)").matches) {
            $('header').fadeOut(100)
            $('.cont-products').fadeOut(100)
        }
        if (window.matchMedia("(min-width: 768px)").matches) {
            $overlay.addClass('active')
        }
    })

    // ПРИ ЗАКРЫТИИ КАРТОЧКИ ТОВАРА
    $(document).on('click', '.btn-close-product-card, .overlay', function() {
        $productCard.removeClass('active')
        if (window.matchMedia("(max-width: 767px)").matches) {
            $('header').fadeIn()
            $('.cont-products').fadeIn()
        }
        if (window.matchMedia("(min-width: 768px)").matches) {
            if($search_popup.hasClass('active') == false) {
                $overlay.removeClass('active')
            } else {
                $search_form.addClass('active')
            }
        }
        // удаление owl-карусели
        $galleryProduct.trigger('destroy.owl.carousel')
        // очистка от изображений
        $galleryProduct.html('')
        // очистка табов продукта
        $productCardTabsContent.each(function() {
            $(this).html('')
        })
    })

    
    // для БОЛЬШИХ экранов
    if (window.matchMedia("(min-width: 768px)").matches) {
        // сайдбар с продуктами всегда активный на больших экранах
        sidebarProducts.addClass('active')

        $('.main-group-products_item__empty').remove()

        // на больших экранах перенастраиваю отображение продуктов по клику
        show_list_subgroup()
        $(document).on('click', '.main-group-products_item', function() {
            $list_group_item = $('.main-group-products_item');
            var title_group = $(this).find('p').html()
            localStorage.setItem('title_group', title_group)

            $list_group_item.each(function() {
                $(this).removeClass('main-group-products_item__active-desc');
            })
            $(this).addClass('main-group-products_item__active-desc')
            var id_group_product_active = $(this).attr('data-id_group')
            localStorage.setItem('id_group', id_group_product_active)
            show_list_subgroup()
        })
    }

    // ПАРСИНГ ТОВАРОВ ПО САБГРУППЕ
    // - получение списка товаров по апи
    // - вывод в разметку
    function parsProductS () {
        var formData = new FormData() // обнуляю формдату
        $.ajax({
            type: "GET",
            url: DOMAIN_PORT + "/api/v1/catalog/product/subgroup_" + localStorage.id_subgroup,
            cache: false,
            dataType: "json",
            contentType: false,
            processData: false,
            data: formData,

            complete: function (data) {
                status = data.status;
                if (status == 200) {
                    const json = data.responseText;
                    const obj = $.parseJSON(json);

                    var count = obj.count_products
                    var items = obj.result
                    if (count > 0) {
                        var div = document.createElement('div')
                        for (var i = 0; i < count; i++) {
                            var id = items[i].id
                            var alias = items[i].alias
                            var title_ru = items[i].title_ru
                            var photo = DOMAIN_PORT + '/media/' + items[i].photo
    
                            var slider_products_item = document.createElement('div')
                            $(slider_products_item).addClass('slider-products-item')
                            $(slider_products_item).attr('data-product_alias', alias).attr('data-product-id', id)
                            $(slider_products_item).html('<div class="slider-products-item--bg"><img src="' + photo + '" alt=""></div><p>' + title_ru + '</p>')
                            div.append(slider_products_item)
                        }  
                        sliderProducts.html($(div).html())
                        if (window.matchMedia("(max-width: 767px)").matches) {
                            initSliderProducts(sliderProducts)
                        }                  

                    } else {
                        var not_product_window_elem = document.createElement('div')
                        $(not_product_window_elem).addClass('not-product-window')
                        $(not_product_window_elem).html('<p class="title-window">В группе "'+localStorage['title_subgroup']+'" еще нету изделий</p>')
                        $(not_product_window_elem).append('<p>' + items + '</p>')
                        sliderProducts.html(not_product_window_elem)
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
                editTextMessage('Произошла ошибка', 'An error has occurred')
            }
        })
        return false
    }

    // ПАРСИНГ ДАННЫХ ТОВАРА
    // - название товара
    // - слайды
    // - описание, состав, приготовление
    function parsProduct () {
        var formData = new FormData() // обнуляю формдату
        $.ajax({
            type: "GET",
            url: DOMAIN_PORT +"/api/v1/catalog/product/id_" + localStorage.id_product,
            cache: false,
            dataType: "json",
            contentType: false,
            processData: false,
            data: formData,

            complete: function (data) {
                status = data.status;
                if (status == 200) {
                    const json = data.responseText;
                    const obj = $.parseJSON(json);

                    // выборка данных
                    var Full_titile_ru = obj.product[0].full_title_ru
                    var arrImgSlids = obj.slide
                    var Description = obj.description[0].description
                    var Composition = obj.composition[0].description
                    var Cooking = obj.cooking[0].description

                    // вывод полного названия товара
                    $productCard.find('.product-card-title').html(Full_titile_ru)

                    // вывод галереии изображений товара
                    if(arrImgSlids.length > 0) {
                        for(i=0; i < arrImgSlids.length; i++) {
                            var galleryProductItem = document.createElement('div')
                            $(galleryProductItem).addClass('gallery-product--item')
                            $(galleryProductItem).html('<img src="'+ DOMAIN_PORT +'/media/' + arrImgSlids[i].photo + '" alt="'+ arrImgSlids[i].alt_text +'">')
                            $galleryProduct.append(galleryProductItem)
                        }
                        initGaleryProduct()
                    }

                    // вывод описания товара
                    if(Description) {
                        var descrItem = document.createElement('p')
                        $(descrItem).html(Description)
                        $productCardTabsContent[0].append(descrItem)
                    }

                    // вывод состава товара
                    if(Composition) {
                        var list_composition = Composition.split('- ')
                        var composListItem = document.createElement('ul')
                        for(i = 0; i < list_composition.length; i++) {
                            var composItem = document.createElement('li')
                            $(composItem).html(list_composition[i])
                            $(composListItem).append(composItem)
                        }
                        $($productCardTabsContent[1]).html(composListItem)
                    }

                    // вывод приготовления товара
                    if(Cooking) {
                        var cookItem = document.createElement('p')
                        $(cookItem).html(Cooking)
                        $productCardTabsContent[2].append(cookItem)
                    }

                    // инициализация таба после наполнения контентом
                    initTabs()

                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
                editTextMessage('Произошла ошибка', 'An error has occurred')
            }
        })
        return false
    }

    //for ios Form Styler
    // $('.jq-selectbox__select-text').on('click', function () {
    //     $('.jq-selectbox__dropdown').toggleClass('block')
    // });
    // $('.jq-selectbox__dropdown li').on('click', function () {
    //     $('.jq-selectbox__dropdown').toggleClass('block')
    // });


    // событие resize - размера окна
    // $(window).resize(function () {
    //     if ($('.class-elem')) {
    //         ...code....
    //     }
    // });
    // отмена события resize на мобильных устройства (так как resize срабатывает даже при обычном скролле)
    // if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    //     $(window).unbind('resize');
    // };


    // событие Смены положения устройства (вертикально-горизонтально)
    // $(window).on("orientationchange", function () {
    //     if (valueOrientation == "portrait") {
    //         // ...code...
    //     }

    //     if (valueOrientation == "landscape") {
    //         // ...code...
    //     }
    // }


    // получение GET-строки и ее параметров (если есть)
    // например http://url.page?id=23&code=55
    // var $_GET = getQueryParams(document.location.search)
    // var id = $_GET['id'] // 23
    // var code = $_GET['code'] // 55


    // работа с глобальным объектом localStorage
    // можно получать доступ к его значениям (одним и тем же) с разных страниц
    // можно прокидывать данные
    // проверка если не пустой
    // if (typeof localStorage['langInterfaceUser'] !== 'undefined') {

    // } else {

    // }
    // // создать элемент (или перезаписать значение) с ключем param в глобальном обьекте
    // localStorage.setItem('param', 'value_param')
    // // получить значение элемента с ключем param (если такое есть)
    // localStorage.getItem('param', 'value_param')

    

    // for placeholder
    $('input, textarea').each(function(){
        var placeholder = $(this).attr('placeholder');
        $(this).focus(function(){ $(this).attr('placeholder', '');});
        $(this).focusout(function(){
            $(this).attr('placeholder', placeholder);
        });
    });

    //checkbox
    var $checkbox = $('input[type=checkbox]');
    $checkbox.css("opacity", "0");
    $checkbox.wrap("<div class='checkbox-wrap'></div>");
    $checkbox.before("<div class='checkbox'></div>");
    $checkbox.click(function(){

        var bbb = $(this).parent().find('.checkbox-checked-base');
        if( bbb.length ){
            $(this).parent().find('.checkbox').removeClass('checkbox-checked-base');
        } else{
            $(this).parent().find('.checkbox').addClass('checkbox-checked-base');
        }
    });

    var $checkboxFiltr = $('.wrap-filter-check, .wrap-check-disability').find('input[type=checkbox]');
    $checkboxFiltr.css("opacity", "0");
    $checkboxFiltr.wrap("<div class='checkbox-wrap'></div>");
    $checkboxFiltr.before("<div class='checkbox'></div>");
    $checkboxFiltr.click(function(){

        var bbb = $(this).parent().find('.checkbox-checked');
        if( bbb.length ){
            $(this).parent().find('.checkbox').removeClass('checkbox-checked');
        } else{
            $(this).parent().find('.checkbox').addClass('checkbox-checked');
        }
    });


    //-----for scroll to top------
    // hide #back-top first
    $(".scrolltop").hide();

    // fade in #back-top
    $(window).scroll(function () {
        if ($(this).scrollTop() > 500) {
            $('.scrolltop').fadeIn();
        } else {
            $('.scrolltop').fadeOut();
        }
    });

    // scroll body to 0px on click
    $('.scrolltop').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 800);
    });
    //-----END for scroll to top------

    


});
